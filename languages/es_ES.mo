��    M      �  g   �      �  
   �     �  	   �     �     �  )   �  &   �          $  !   ,     N     W     \     o     r     w  	   �     �     �     �     �     �     �     �       	             .  2   F  	   y     �     �     �     �     �     �     �     �     �     �     �     	     #	  .   ,	     [	     s	     �	     �	     �	     �	     �	     �	  
   
     
     $
     -
     >
     L
     R
  
   Y
     d
     t
     �
     �
     �
     �
     �
  �   �
     �     �     �     �  J   �  %     D   C     �  }  �     	          +     8     <  +   H  (   t     �     �  '   �     �     �     �     	       /     	   D     N     T     l     �     �     �     �     �     �     �       :   $     _     o  "   u  	   �     �     �     �     �     �  	          #        >     W  ;   `     �  1   �  &   �          /     N     i     q     �     �     �     �     �  	   �  	   �     �                (  !   =     _  
   f     q  �   �  	   `  
   j     u     �  S   �  )   �  R   !     t           "   -   8       /   .   !                 +           <   H   0   @         	   4       M      #          )       ,   B   :      6          &      F              3               7   ;       E      G   C                  1      >      D      9         L   (                  $   5   =   *   
       J         ?         K      %       2             I       A   '               % Comments . You can read it on:  1 Comment 404 404 Page <span class="%1$s">Your search for "%2$s" <span class="title-text">Cancel</span> About the author Archive At least {0} characters required. BREAKING Blog Blog Right Sidebar By Edit Email <span>(required):</span> Error 404 Follow: Footer Menu Go back to previous page Go to homepage Home I recommend this page:  Latest Posts Leave a reply Location: Log out &raquo; Log out of this account Logged in as <a href="%1$s" title="%2$s">%2$s</a>. Main Menu Menu Name <span>(required):</span> Next Next Article No Comments Oops! errors occured. Override Title: Page Page % Page %s Page Right Sidebar Page not found... Pingback Please do not load this page directly. Thanks! Please enter a message. Please enter a valid email. Please enter a valid url. Please enter your email. Please enter your name. Please enter your url. Post Post Comment Posted by: Posts created by %1$s Previous Previous Article Related Posts Reply Search Sending... Share this Post Sidebar Manager Single Right Sidebar Sorry an error has occurred. Submit Taxonomy Type your search here We're sorry, but we can't find the page you were looking for. It's probably some thing we've done wrong but now we know about it we'll try to fix it. In the meantime, try one of this options: Website Website: You are here: You cannot vote twice. You must be <a href="%1$s" title="Log in">logged in</a> to post a comment. Your comment <span>(required):</span> Your email address will not be published. Required fields are marked at Project-Id-Version: Forceful Lite v1.0.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2014-11-27 16:29-0400
Last-Translator: Benito Anagua <benito@uajms.edu.bo>
Language-Team: Spanish <kde-i18n-doc@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.6.10
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
Language: es
X-Poedit-SearchPath-0: .
 % Comentarios . Se puede leer en: 1 comentario 404 404 página <span class="%1$s">Tu búsqueda para "%2$s" <span class="title-text">Cancelar</span> Sobre el autor Archivo Por lo menos {0} caracteres requeridos. NOTICIAS Blog Panel lateral derecho del blog Por Editar Correo electrónico <span>(obligatorio):</span> Error 404 Siga: Menú de pie de página Volver a la página anterior Ir a página principal Página de inicio Te recomiendo esta página: Últimos mensajes Contesta Ubicación: Cerrar sesión &raquo; Registro de esta cuenta Iniciar sesión como <a href="%1$s" title="%2$s">%2$s</a>. Menú principal Menú Nombre <span>(obligatorio):</span> Siguiente Siguiente artículo No hay comentarios ¡ Uy! producido errores. Reemplazar el título: Página Página % Página de %s Barra lateral derecha de la página Página no encontrada... Pingback Por favor, no cargue esta página directamente. ¡ Gracias! Por favor ingrese un mensaje. Por favor escriba un correo electrónico válido. Por favor, introduzca una url válida. Por favor ingresa tu email. Por favor introduce tu nombre. Escriba su dirección url. Mensaje Enviar comentario Publicado por: Mensajes creados por %1$s Anterior Artículo anterior Artículos Relacionados Respuesta Búsqueda Enviando... Comparte este artículo Sidebar Manager Solo lateral derecha Lo sentimos ha ocurrido un error. Enviar Taxonomía Escriba aquí su búsqueda Lo sentimos, pero no podemos encontrar la página que estabas buscando. Es probablemente algo que hemos hecho mal, pero ahora sabemos que intentaremos arreglarlo. En Mientras tanto, pruebe una de estas opciones: Sitio web Sitio web: Usted está aquí: No puedes votar dos veces. Debes estar <a href="%1$s" title="Log in"> logueado</a> para publicar uncomentario. Tu comentario <span>(obligatorio):</span> Su dirección de email no será publicada. Los campos obligatorios están marcados en 