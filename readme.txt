Forceful theme
--------------------

Description: Forceful theme is designed for magazine, news with flexible layout. The theme is based on KOPATHEME layout manager technique that will let you flexibility choose layout options of every pages within your site. It is very helpful when you are experimenting with visual hierarchy. You can define unlimited sidebar for widget areas, and with powerful custom widgets, the theme provides you more flexibility and ease-of-use for your site

--------------------
Sources and Credits
--------------------

--------------------
Font Awesome License
--------------------

Font License - http://fontawesome.io
License: SIL OFL 1.1
License URI: http://scripts.sil.org/OFL
Copyright: Dave Gandy, http://fontawesome.io

Code License - http://fontawesome.io
License: MIT License
License URI: http://opensource.org/licenses/mit-license.html
Copyright: Dave Gandy, http://fontawesome.io

Brand Icons
All brand icons are trademarks of their respective owners.
The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.


----------------------------
License/copyright for images
----------------------------

* Images are used in the screenshot are creative common license 
http://pixabay.com/en/tiger-animal-wild-wildlife-nature-484097/
http://pixabay.com/en/blue-angels-jets-f-18-flight-485331/
http://pixabay.com/en/girl-cold-freeze-woman-hat-winter-15715/



* Otherwise images are licensed under GNU General Public License version 3, see file license.txt


--------------------
Other Licenses
--------------------
See headers of files for further details.

Thanks!
KOPASOFT
